// Import node modules
import { version } from '../../package.json';
import { Router } from 'express';
import {validateBoundingBoxQuery, validateGetFiltersQuery, isAlphaNumeric} from '../services/validator'
import formatter from '../services/formatter'
import production from '../services/production'
import dotenv from 'dotenv'
import request from 'request-promise'

// configure .env, MUST be done before environment variables are accessed
dotenv.config()

// Constants
const DEFAULT_OFFSET = 0
const DEFAULT_LIMIT = 10
const MAX_PAGE_LIMIT = 10000
const ELASTICSEARCH_BASE_URL = process.env.ESURL
const GEO_QUERY_ATTRIBUTE_KEY = 'CoordinatesGP'
const TRANSACTIONS_URL = `${ELASTICSEARCH_BASE_URL}/transactions/transaction/_search`
const MAX_COUNT_UNIQUE_KWUIDS = 117000
const SPECIALIZATIONS_INDEX = 'specialization'
const SPECIALIZATIONS_TYPE = 'member'
const FILTERS_INDEX = 'filters'
const FILTERS_TYPE = 'filter_id'
const DB_INDEX = 'transactions'
const DB_TYPE = 'transaction'

// API to execute bounding box search query
export default ({ config, db }) => {
  let api = Router();

  api.get('/bounding_box/filters', async (req, res) => {
    // Validate input
    let validationResult = validateGetFiltersQuery(req.query)
    if (!validationResult.valid) {
      return res.status(400).send({status: 400, error: validationResult.error})
    }

    // Grab variables
    let {page} = req.query
    let pageNum = page && page.offset ? page.offset - 1 : 0
    let pageSize = page && page.limit || 10

    try {
      let filterResponse = await production.listFilters(pageNum, pageSize)
      let formattedResponse = formatter.formatProductionResponse(filterResponse)
      return res.send(formattedResponse)

    } catch(e) {
      console.log(Date.now() + e.message)
      return res.status(400).send({status: 400, error: e.message})
    }
  })

  api.get('/bounding_box/filters/:filter_id', async (req, res) => {
    let { filter_id: filterId } = req.params

    // validate filter id
    if (typeof filterId !== 'string' || !isAlphaNumeric(filterId)) {
      return res.status(400).send({status: 400, error: 'filter_id must be an alphanumeric string'})
    }

    try {
      let response = await production.listFilter(filterId)
      return res.send(response)
    } catch (e) {
      console.log(e)
      return res.status(400).send({status: 400, error: e.message})
    }
  })

  // GET /bounding_box
  // Returns listings within top_left, bottom_right geographic box
  api.get('/bounding_box', async (req, res) => {
    let {top_left, bottom_right, closed_units, listings_sold_u, buy_sales_closed_u, lease_closed_u, memberships, events, sort, page} = req.query
    let offset = DEFAULT_OFFSET
    let limit = DEFAULT_LIMIT

    let sortString
    if (sort)
      sortString = sort.charAt(0) === '-' ? `${sort.substr(1)}:desc` : sort

    // Assign paging variables
    if (page && page.offset) offset = page.offset
    if (page && page.limit) {
      limit = page.limit
      if (page.limit > MAX_PAGE_LIMIT) {
        limit = MAX_PAGE_LIMIT
      }
    }

    // Validate request
    let validationResult = validateBoundingBoxQuery(req.query)
    if (!validationResult.valid) {
      return res.status(400).send({error: validationResult.error, status: 400})
    }

    // Default body
    let body = {
      "_source": ["KwUID", "CoordinatesGP"],
      "query": {
        "bool": {
          "filter": [
            {
              "range": {
                "ClosedDate": {
                  "gte": "now-1y-30d"
                }
              }
            }
          ]
        }
      },
      "aggs": {
        "closed_units": {
          "terms": {
            "field": "KwUID",
            "size": MAX_COUNT_UNIQUE_KWUIDS
          }
        },
        "listings_sold": {
          "filter": {
            "term": {
              "DaType.keyword": "L"
            }
          },
          "aggs": {
            "kwuids": {
              "terms": {
                "field": "KwUID",
                "size": MAX_COUNT_UNIQUE_KWUIDS,
              }
            }
          }
        },
        "buy_sides": {
          "filter": {
            "term": {
              "DaType.keyword": "S"
            }
          },
          "aggs": {
            "kwuids": {
              "terms": {
                "field": "KwUID",
                "size": MAX_COUNT_UNIQUE_KWUIDS,
              }
            }
          }
        }
      }
    }

    // Add Bounding box filter
    if (top_left && bottom_right) {
      body.query.bool.filter.push({
        geo_polygon: {
          [GEO_QUERY_ATTRIBUTE_KEY]: {
            points: [
              {
                lat: top_left.lat,
                lon: top_left.lon
              },
              {
                lat: top_left.lat,
                lon: bottom_right.lon
              },
              {
                lat: bottom_right.lat,
                lon: bottom_right.lon
              },
              {
                lat: bottom_right.lat,
                lon: top_left.lon
              },
              {
                lat: top_left.lat,
                lon: top_left.lon
              }
            ]
          }
        }
      })
    }
    try {
      const closedUnitsExistsAndIsGreaterThanOne = closed_units && closed_units > 1

      if ((closedUnitsExistsAndIsGreaterThanOne) || listings_sold_u || buy_sales_closed_u || lease_closed_u) { // kwuid is only present if they have at least one closed unit by definition

        // Add agg params to body
        let aggBody = JSON.parse(JSON.stringify(body))
        aggBody.size = 0
        aggBody.aggs = {}

        if (closedUnitsExistsAndIsGreaterThanOne) {
          aggBody["aggs"]["closed_units"] = {
            "terms": {
              "field": "KwUID",
              "size": MAX_COUNT_UNIQUE_KWUIDS,
              "min_doc_count": closed_units,
              "order" : { "_key" : "asc" }
            }
          }
        }

        if (listings_sold_u) {
          aggBody["aggs"]["listings_sold_u"] = {
            "filter": {
              "term": {
                "DaType.keyword": "L"
              }
            },
            "aggs": {
              "kwuids": {
                "terms": {
                  "field": "KwUID",
                  "size": MAX_COUNT_UNIQUE_KWUIDS,
                  "min_doc_count": listings_sold_u,
                  "order" : { "_key" : "asc" }
                }
              }
            }
          }
        }

        if (buy_sales_closed_u) {
          aggBody["aggs"]["buy_sales_closed_u"] = {
            "filter": {
              "term": {
                "DaType.keyword": "S"
              }
            },
            "aggs": {
              "kwuids": {
                "terms": {
                  "field": "KwUID",
                  "size": MAX_COUNT_UNIQUE_KWUIDS,
                  "min_doc_count": buy_sales_closed_u,
                  "order" : { "_key" : "asc" }
                }
              }
            }
          }
        }

        if (lease_closed_u) {
          aggBody["aggs"]["lease_closed_u"] = {
            "filter": {
              "term": {
                "Class.keyword": "L"
              }
            },
            "aggs": {
              "kwuids": {
                "terms": {
                  "field": "KwUID",
                  "size": MAX_COUNT_UNIQUE_KWUIDS,
                  "min_doc_count": lease_closed_u,
                  "order" : { "_key" : "asc" }
                }
              }
            }
          }
        }

        // Run agg query
        let startTimeFirstQuery = Date.now()
        let aggResponse = await db.search({
          index: DB_INDEX,
          type: DB_TYPE,
          body: JSON.stringify(aggBody)
        })
        let endtimeFirstQuery = Date.now()
        console.log(`Aggregations query took ${endtimeFirstQuery - startTimeFirstQuery} ms`)

        // Get the intersection of filter aggregation kwuid arrays
        let kwuids, closedUnitsKwuids, listingsSoldKwuids, buySalesClosedKwuids, leasesKwuids = []

        closedUnitsKwuids = (closedUnitsExistsAndIsGreaterThanOne) ? aggResponse.aggregations.closed_units.buckets.map(doc => doc.key) : []
        listingsSoldKwuids = listings_sold_u ? aggResponse.aggregations.listings_sold_u.kwuids.buckets.map(doc => doc.key) : []
        buySalesClosedKwuids = buy_sales_closed_u ? aggResponse.aggregations.buy_sales_closed_u.kwuids.buckets.map(doc => doc.key) : []
        leasesKwuids = lease_closed_u ? aggResponse.aggregations.lease_closed_u.kwuids.buckets.map(doc => doc.key) : []

        kwuids = closedUnitsKwuids

        if (listings_sold_u && !kwuids.length) {
          kwuids = listingsSoldKwuids
        } else if (listings_sold_u) {
          kwuids = intersectArrays(kwuids, listingsSoldKwuids)
        }

        if (buy_sales_closed_u && !kwuids.length) {
          kwuids = buySalesClosedKwuids
        } else if (buy_sales_closed_u) {
          kwuids = intersectArrays(kwuids, buySalesClosedKwuids)
        }

        if (lease_closed_u && !kwuids.length) {
          kwuids = leasesKwuids
        } else if (lease_closed_u) {
          kwuids = intersectArrays(kwuids, leasesKwuids)
        }

        body.query.bool.filter.push({terms: { "KwUID": kwuids }})
        if (kwuids.length) {
          body.aggs.closed_units.terms.size = kwuids.length
          body.aggs.listings_sold.aggs.kwuids.terms.size = kwuids.length
          body.aggs.buy_sides.aggs.kwuids.terms.size = kwuids.length
        }
      }

      // Check for kw specializations/memberships
      if (memberships) {
        let kwuids = []
        let membershipsList = memberships.split(',')
        for (let membershipId of membershipsList) {
          let membershipRes = await db.get({
            index: SPECIALIZATIONS_INDEX,
            type: SPECIALIZATIONS_TYPE,
            id: membershipId
          })
          if (membershipRes._source) {
            kwuids = kwuids.concat(membershipRes._source.list_kwuid)
          }
        }
        if (kwuids.length) {
          body.query.bool.filter.push({terms: { "KwUID": kwuids }})
        }
      }

      // Check generic filters index, looking for events specifically,
      // at time of implementation this index only held event registration
      if (events) {
        let kwuids = []
        let eventsList = events.split(',')
        for (let eventId of eventsList) {
          let eventRes = await db.get({
            index: FILTERS_INDEX,
            type: FILTERS_TYPE,
            id: eventId
          })
          if (eventRes._source) {
            kwuids = kwuids.concat(eventRes._source.list_kwuid)
          }
        }
        if (kwuids.length) {
          body.query.bool.filter.push({terms: { "KwUID": kwuids }})
        }
      }


      let startTimeSecondQuery = Date.now()
      let response = await db.search({
        index: DB_INDEX,
        type: DB_TYPE,
        body: JSON.stringify(body),
        filter_path: 'took,hits.total,hits.hits._id,hits.hits._source,aggregations',
        from: offset,
        size: limit,
        sort: sortString
      })
      let endTimeSecondQuery = Date.now()
      console.log(`Results query took ${endTimeSecondQuery - startTimeSecondQuery} ms`)
      if (response.hits && response.hits.total === 0) {
        return res.send({meta: {total: response.hits.total, count: response.hits.total}, data: []})
      } else if (response.hits && response.hits.hits) {
        let results = response.hits.hits
        let total = response.hits.total
        return res.send({meta: {total, count: results.length}, data: results, aggregations: response.aggregations})
      }

    } catch (error) {
      return res.status(400).send({status: 400, error: error.message})
    }
});

  return api;
}

function intersectArrays(a, b) {
  let res = []
  let setA = new Set(a)

  for (let i in b) {
    let item = b[i]
    if (setA.has(item)) {
      res.push(item)
    }
  }

  return res
}
