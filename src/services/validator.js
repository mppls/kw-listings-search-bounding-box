let schema = '{"_id":"404716","KwUID":289972,"CoordinatesGP":{"lon":-105.14862,"lat":39.69049}}'

class ReferralsNowGeoQueryValidator {
  static validateBoundingBoxQuery(query) {
    let {top_left, bottom_right, closed_units, listings_sold_u, buy_sales_closed_u, lease_closed_u, memberships, events, sort, page} = query

    // Validate coordinate existence
    if (!top_left) {
      return {valid: false, error: 'missing parameter top_left'}
    }
    if (!bottom_right) {
      return {valid: false, error: 'missing parameter bottom_right'}
    }

    if (!top_left.lat)  {
      return {valid: false, error: 'missing parameter top_left.lat'}
    }
    if (!top_left.lon) {
      return {valid: false, error: 'missing parameter top_left.lon'}
    }

    if (!bottom_right.lat) {
      return {valid: false, error: 'missing parameter bottom_right.lat'}
    }
    if (!bottom_right.lon) {
      return {valid: false, error: 'missing parameter bottom_right.lon'}
    }

    // Validate coordinate types and values
    let topLeftLat = parseFloat(top_left.lat)
    if (isNaN(topLeftLat) || topLeftLat < -90 || topLeftLat > 90) {
      return {valid: false, error: `top_left.lat must be a integer between -90 and 90`}
    }

    let topLeftLon = parseFloat(top_left.lon)
    if (isNaN(topLeftLon) || topLeftLon < -180 || topLeftLon > 180) {
      return {valid: false, error: `top_left.lon must be an integer between -180 and 180`}
    }

    let bottomRightLat = parseFloat(bottom_right.lat)
    if (isNaN(bottomRightLat) || bottomRightLat < -90 || bottomRightLat > 90) {
      return {valid: false, error: `bottom_right.lat must be an integer between -90 and 90`}
    }

    let bottomRightLon = parseFloat(bottom_right.lon)
    if (isNaN(bottomRightLon) || bottomRightLon < -180 || bottomRightLon > 180) {
      return {valid: false, error: 'bottom_right.lon must be an integer between -180 and 180'}
    }

    // Validate min agent count filters
    if (closed_units && isNaN(closed_units)) {
      return { valid: false, error: `closed_units must be an integer` }
    }
    if (listings_sold_u && isNaN(listings_sold_u)) {
      return { valid: false, error: `listings_sold_u must be an integer` }
    }
    if (buy_sales_closed_u && isNaN(buy_sales_closed_u)) {
      return { valid: false, error: `buy_sales_closed_u must be an integer` }
    }
    if (lease_closed_u && isNaN(lease_closed_u)) {
      return { valid: false, error: `lease_closed_u must be an integer` }
    }

    // Validate kw specializations/memberships
    if (memberships) {
      let arr = memberships.split(',')
      for (let i = 0; i < arr.length; i++) {
        if (isNaN(parseInt(arr[i]))) {
          return { valid: false, error: 'all memberships ids must be integers'}
        }
      }
    }

    // Validate event registration
    if (events) {
      let arr = events.split(',')
      for (let i = 0; i < arr.length; i++) {
        if (!ReferralsNowGeoQueryValidator.isAlphaNumeric(arr[i])) {
          console.log(1)
          return { valid: false, error: 'all memberships ids must be integers'}
        }
      }
    }

    // Validate offset and limit
    if (page) {
      if (page.offset && (isNaN(parseInt(page.offset)) || parseInt(page.offset) < 0)) {
        return {valid: false, error: 'parameter offset must be a non-negative integer'}
      }

      if (page.limit && (isNaN(parseInt(page.limit)) || parseInt(page.limit) < 0)) {
        return {valid: false, error: 'parameter limit must be an non-negative integer'}
      }
    }

    // Validate sort param
    if (sort && sort.length) {
      let schemaObj = JSON.parse(schema)
      let sortField = sort.charAt(0) === '-' ? sort.substr(1) : sort

      if (!schemaObj.hasOwnProperty(sortField)) {
        return {valid: false, error: 'invalid sort parameter ' + sortField}
      }
    }

    return {valid: true}
  }

  static validateGetFiltersQuery(query) {
    let {page} = query

    // Validate offset and limit
    if (page) {
      if (page.offset && (isNaN(parseInt(page.offset)) || parseInt(page.offset) < 0)) {
        return {valid: false, error: 'parameter offset must be a non-negative integer'}
      }

      if (page.limit && (isNaN(parseInt(page.limit)) || parseInt(page.limit) < 0)) {
        return {valid: false, error: 'parameter limit must be an non-negative integer'}
      }
    }

    return {valid: true}
  }

  static isAlphaNumeric(str) {
    let code, i, len

    for (i = 0, len = str.length; i < len; i++) {
      code = str.charCodeAt(i)
      if (!(code > 47 && code < 58) && // numeric (0-9)
        !(code > 64 && code < 91) && // upper alpha (A-Z)
        !(code > 96 && code < 123)) { // lower alpha (a-z)
        return false
      }
    }
    return true
  }
}

module.exports = ReferralsNowGeoQueryValidator
