// Import node modules
import elasticsearch from 'elasticsearch'
import babelPolyfill from 'babel-polyfill'

// Define class
class Production {
  static async listFilters(pageNum, pageSize) {
    let client = new elasticsearch.Client({host: process.env.ESURL})
    let query = getAllFiltersQuery()

    return await client.search({
      index: 'filters',
      type: 'filter_id',
      body: JSON.stringify(query),
      _source: ['filter_id', 'filter_type', 'filter_name', 'in_use'],
      filter_path: 'took, hits.total, hits.hits._id, hits.hits._source.filter_name, hits.hits._source.filter_type',
      from: pageNum,
      size: pageSize,
    })
  }

  static async listFilter(filter) {
    let client = new elasticsearch.Client({host: process.env.ESURL})

    return await client.get({
      index: 'filters',
      type: 'filter_id',
      id: filter,
      _source: ['filter_id', 'filter_type', 'filter_name', 'list_kwuid'],
      filter_path: 'took, _id, _source'
    })
  }
}

/**
 * Create query to get all
 * @returns {{query: {bool: {must: Array}}}}
 */
function getAllFiltersQuery() {
  let q = getEmptyQuery()
  q.query.bool.must.push({term: {"filter_type": 'event'}})
  q.query.bool.must.push({term: {"in_use": true}})
  return q
}

/**
 * Create base query object
 * @returns {{query: {bool: {must: Array}}}}
 */
function getEmptyQuery() {
  return {query: {bool: {must: []}}}
}

// Export class
export default Production
