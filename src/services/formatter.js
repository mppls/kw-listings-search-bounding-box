// Declare class
class JSONAPIFormatter {
  static formatProductionResponse(response) {
    let total = response.hits.total
    let hits = response.hits.hits || []
    // let data = hits.map(item => {
    //   return {...item._source}
    // })
    let data = hits

    return {
      meta: {
        count: hits.length,
        total,
      },
      data
    }
  }

  static formatFilterResponse(response) {}
}

// Export class
export default JSONAPIFormatter
