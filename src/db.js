// Import node modules
import elasticsearch from 'elasticsearch'
import dotenv from 'dotenv'

// Connect to a database, then pass it to `callback`
export default callback => {
  dotenv.config()
  const client = elasticsearch.Client({
    host: process.env.ESURL
  })
  callback(client)
}
